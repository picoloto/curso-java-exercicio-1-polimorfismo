package aplicacao;

import entidades.Conta;
import entidades.ContaEmpresa;

public class Programa {

	public static void main(String[] args) {
		
		Conta c = new Conta(1001, "Leandro", 1000.0);
		Conta c1 = new ContaEmpresa(1002, "Pamela", 1000.00, 5000.00);
		
		c.saque(50.0);
		c1.saque(50.0);
		
		System.out.println(c.getSaldo());
		System.out.println(c1.getSaldo());

	}

}
